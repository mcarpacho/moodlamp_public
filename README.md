# moodLamp_public

Mood Lamp project. It is intended to drive RGB leds depending on the mood of the ambient. Basically it will capture the sound level, and drive the RGB led board with the color representing the mood: Blue -> quiet ambient; Red-> very noisy.